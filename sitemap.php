<?php 
header("Content-Type: application/xml; charset=utf-8"); 
echo '<?xml version="1.0" encoding="UTF-8" ?>'.PHP_EOL; 
echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">' .PHP_EOL; 
 
# attempt to scan page dynamically, abandoned because the page order was wrong:
# $allpages = glob("core/*.php");
# $allmodules = glob("core/mod-*.php");
# $alllibraries = glob("core/lib-*.php");
# $manuallyremoved = array( 0 => "core/404.php", 1 => "core/viewer.php", 2 => "core/xyz.php" );
# Substract the total allpages from modules, libraries and manually removed.
# $result = array_diff($allpages, $allmodules);
# $result = array_diff($result, $alllibraries);
# $result = array_diff($result, $manuallyremoved);

$sources = '0_sources';
$cache = 'cache';

$file_root = dirname(__FILE__).'/';
include($file_root.'core/lib-database.php');

$pagelist = array( 
                  0 => "homepage",
                  1 => "webcomics",
                  2 => "artworks",
                  3 => "goodies",
                  4 => "wallpapers",
                  5 => "contribute",
                  6 => "fan-art",
                  7 => "documentation",
                  8 => "wiki",
                  9 => "about",
                  10 => "license"
                );

function xml_page_node($page) {
  global $languages_available;
  echo '  <url>'.PHP_EOL; 
  echo '    <loc>https://www.peppercarrot.com/en/'.$page.'/index.html</loc>'. PHP_EOL; 
  foreach ($languages_available as $lang) {
    echo '    <xhtml:link href="https://www.peppercarrot.com/'.$lang.'/'.$page.'/index.html" hreflang="'.$lang.'" rel="alternate" />'. PHP_EOL;
  }
  echo '    <changefreq>weekly</changefreq>'.PHP_EOL; 
  echo '  </url>'.PHP_EOL;
}

foreach ($pagelist as $page) {
  xml_page_node($page);
}

echo '</urlset>'; 
?>
