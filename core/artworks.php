<?php if ($root=="") exit; 

# Initiate page
echo '<div class="container">'."\n";

# Include the webcomic sources 
include($file_root.'core/mod-menu-lang.php');
include($file_root.'core/lib-gallery.php');

# Sidebar
# -------
echo '  <aside class="wikivertimenu col sml-12 med-12 lrg-2" role="complementary">'."\n";
echo '    <nav class="wikibuttonmenu col sml-12 med-12 lrg-12" style="padding:0 0;">'."\n";

# Array of directories available
$sources_directories = glob($sources.'/0ther/*');
sort($sources_directories);

$dirclass = '';

# Menu
if ($content == 'artworks') { $dirclass = ' active'; } else { $dirclass = ' no-active'; }
echo '      <a class="artbutton artworks'.$dirclass.'" href="'.$root.'/'.$lang.'/artworks/artworks.html">'."\n";
echo '        '._("Artworks").''."\n";
echo '      </a>'."\n";

if ($content == 'sketchbook') { $dirclass = ' active'; } else { $dirclass = ' no-active'; }
echo '      <a class="artbutton sketchbook'.$dirclass.'" href="'.$root.'/'.$lang.'/artworks/sketchbook.html">'."\n";
echo '        '._("Sketchbook").''."\n";
echo '      </a>'."\n";

if ($content == 'framasoft') { $dirclass = ' active'; } else { $dirclass = ' no-active'; }
echo '      <a class="artbutton framasoft'.$dirclass.'" href="'.$root.'/'.$lang.'/artworks/framasoft.html">'."\n";
echo '        '._("Framasoft").''."\n";
echo '      </a>'."\n";

if ($content == 'misc') { $dirclass = ' active'; } else { $dirclass = ' no-active'; }
echo '      <a class="artbutton misc'.$dirclass.'" href="'.$root.'/'.$lang.'/artworks/misc.html">'."\n";
echo '        '._("Misc").''."\n";
echo '      </a>'."\n";

echo '      <a class="loadmorebutton" href="'.$root.'/'.$lang.'/files/index.html">'."\n";
echo '        '._("Sources explorer").''."\n";
echo '      </a>'."\n";
echo '    </nav>'."\n";
echo '    <div style="clear:both"></div>'."\n";
echo '  </aside>'."\n";
echo ''."\n";

# Gallery thumbnails
# ------------------
echo '  <article class="col sml-12 med-12 lrg-10 sml-text-center">'."\n";
echo '    <ul id="gallery">'."\n";

_gallery($content);

echo '    </ul>'."\n";
echo '  </article>'."\n";
echo '</div>'."\n";
echo ''."\n";
?>
